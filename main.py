l = [90, 401, 400, 64, 47, 84, 323, 321, 8, 6, 99]


def calc_pivot(list_to_sort) -> int:
    # get first element of list as pivot element
    return list_to_sort[0]


def quicksort(list_to_sort):
    #abbruchbedingung
    if len(list_to_sort) <= 1:
        return list_to_sort

    pivot = calc_pivot(list_to_sort)
    left = []
    right = []



    for index, n in enumerate(list_to_sort):
        if index > 0:
            if n >= pivot:
                right.append(n)
            else:
                left.append(n)

    left_sorted = quicksort(left)
    right_sorted = quicksort(right)
    left_sorted.append(pivot)
    left_sorted += right_sorted
    return left_sorted


print(quicksort(l))
